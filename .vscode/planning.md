## Set up
* [x] Fork and clone the starter project from django-one-shot 
* [x] Create a new virtual environment in the repository directory for the project
  * [x] python -m venv .venv
* [x] Activate the virtual environment
  * [x] source .venv/bin/activate
* [x] Upgrade pip
* [x] Install django
  * [x] pip install django
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8
* [x] Install djlint
  * [x] pip install djlint
* [x] Install Debugger tool
  * [x] pip install django-debug-toolbar
* [x] Use pip freeze to generate a requirements.txt file
  * [x] pip freeze > requirements.txt
* [x] Add and commit progress
  * [x] git add .
  * [x] git commit -m "Feature 1 Complete"
  * [x] git push origin main

--------

## Feature 2
* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
  * [x] django-admin startproject name .
* [x] Create a Django app named todos
  * [x] python manage.py startapp name
* [x] and install it in the brain_two Django project in the INSTALLED_APPS list
  * [x] 'todos.apps.TodosConfig'
* [x] Run the migrations
  * [x] python manage.py makemigrations
  * [x] python manage.py migrate
* [x] Create a super user
  * [x] python manage.py createsuperuser
* [x] Add and commit progress
  * [x] git add .
  * [x] git commit -m "Feature 2 Complete"
  * [x] git push origin main

--------

## Feature 3

* [x] Create a TodoList model in todos Django app
* [x] migrate: python manage.py makemigrations & migrate
* [x] Test this model
  * [x] run django shell: python manage.py shell
  * [x] import model: from.todos.models import TodoList
  * [x] todolist = TodoList.objects.create(name="Reminders")
* [x] Add and commit progress
  * [x] git add .
  * [x] git commit -m "Feature 2 Complete"
  * [x] git push origin main


-----------

## Feature 4

* [x] Register TodoList model in admin
* [x] Open admin browser and add todo list
* [x] Add and commit progress
  * [x] git add .
  * [x] git commit -m "Feature 2 Complete"
  * [x] git push origin main

-----------

## Feature 5

* [x] Create a TodoItem model in the todos django app
  * [x] task
  * [x] due_date
  * [x] is_completed
  * [x] list
  * [x] string method
* [x] makemmigrations and migrate
* [x] Test the model
  * [x] run django shell
  * [x] import the model
  * [x] create a few todoitems
* [x] Add and commit progress
  * [x] git add .
  * [x] git commit -m "Feature 2 Complete"
  * [x] git push origin main


--------------


## Feature 6

* [x] register TodoItem in admin
* [x] test by opening admin browser
* [x] Add and commit progress
  * [x] git add .
  * [x] git commit -m "Feature 2 Complete"
  * [x] git push origin main

------------------


## Feature 7

* [x] Create a view that will get all of the instances of the TodoList model 
  * [x] import model into the views.py
  * [x] import ListView model into views.py (class based view)
  * [x] And put them in the context for the template.
* [x] Create a urls.py file for todos app
  * [x] import path from django.urls
  * [x] import TodoListListView from views.py
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/"
  * [x] import include from django.urls
* [x] Create a template for the list view that complies with the following specifications.
* [x] the fundamental five in it
a main tag that contains:
  * [x] div tag that contains:
  * [x] an h1 tag with the content "My Lists"
  * [x] a table that has two columns:
  * [x] the first with the header "Name" and the rows with the names of the Todo lists
  * [x] the second with the header "Number of items" and nothing in those rows because we don't yet have tasks
* [x] Add and commit progress
  * [x] git add .
  * [x] git commit -m "Feature 2 Complete"
  * [x] git push origin main

--------------------

## Feature 8
* [x] Create a view that shows the details of a particular to-do list, including its tasks
  * [x] from django.views.generic.detail import DetailView
  * [x] from todos.models import TotoItem
* [x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "todo_list_detail"
* [x] Create a template to show the details of the todolist and a table of its to-do items
* [ ] Update the list template to show the number of to-do items for a to-do list
* [ ] Update the list template to have a link from the to-do list name to the detail view for that to-do list
