from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

# Create your views here.


# ListView
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todolistlist"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todolistdetail"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    # success_url = reverse_lazy("todo_list_list")

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/itemcreate.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy(
            "todo_list_detail", kwargs={"pk": self.object.list.pk}
        )


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/updateitem.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy(
            "todo_list_detail", kwargs={"pk": self.object.list.pk}
        )
